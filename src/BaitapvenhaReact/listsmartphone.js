import React from "react";
import Productitemsmartphone from "./productitemsmartphone.js";

export default function Listsmartphone() {
  return (
    <section class="container-fluid pt-5 pb-5 bg-dark">
      <h1 class="text-white text-center">BEST SMARTPHONE</h1>
      <div class="row">
        <Productitemsmartphone />
        <Productitemsmartphone />
        <Productitemsmartphone />
        <Productitemsmartphone />
      </div>
    </section>
  );
}
