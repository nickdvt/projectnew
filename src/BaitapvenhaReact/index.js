import React from "react";
import Header from "./header.js";
import Carousel from "./carousel.js";
import Listsmartphone from "./listsmartphone.js";
import Listlaptop from "./listlaptop.js";
import Footer from "./footer.js";

export default function BaitapvenhaReact() {
  return (
    <div>
      <Header />
      <Carousel />
      <Listsmartphone />
      <Listlaptop />
      <Footer />
    </div>
  );
}
