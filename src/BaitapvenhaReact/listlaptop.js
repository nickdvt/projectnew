import React from "react";
import Productitemlaptop from "./productitemlaptop.js";

export default function Listlaptop() {
  return (
    <section id="laptop" class="container-fluid pt-5 pb-5 bg-light text-dark">
      <h1 class="text-center">BEST LAPTOP</h1>
      <div class="row">
        <Productitemlaptop />
        <Productitemlaptop />
        <Productitemlaptop />
        <Productitemlaptop />
      </div>
    </section>
  );
}
